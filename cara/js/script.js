const form = document.getElementById("form");
const url = "http://localhost:1337/upload_files"
form.addEventListener("submit", submitForm);
var missingFiles = []
// \n = New line 
function submitForm(e) {
  e.preventDefault();
  const files = document.getElementById("files");
  const formData = new FormData();

  if (checkFiles()) {
    //appends files
    for (let i = 0; i < files.files.length; i++) {
      formData.append("files", files.files[i]);
      //console.log(files.files[i])
    }
    fetch(url, {
      method: 'POST',
      body: formData
    })
      .then((res) => {
        alert("The files have been uploaded succesfully!")
        console.log(res)
      }
      )
      .catch((err) => ("Error occured", err));
  } else {
    formattedText = "The following files are missing: \n"
    missingFiles.forEach(file => {
      formattedText = formattedText + " \n" + file
    })
    alert(formattedText)
  }
}
function checkFiles() {
  missingFiles.length = 0 
  var filesNeeded = ["icon.png", "launcher.png", "logo.png", "app-setup.txt", "google-services.json", "GoogleService-Info.plist"]
  var filesUploaded = []
  for (let i = 0; i < files.files.length; i++) {
    console.log(files.files[i].name)
    filesUploaded.push(files.files[i].name)
  }
  //checks if all files are selected
  filesNeeded.forEach(file => {
    if (filesUploaded.includes(file)) {
    } else {
      missingFiles.push(file)
      return false
    }
  })
  if (files.files.length === 0 || files.files.length > 6 ) {
    return false
  }
  return true

}