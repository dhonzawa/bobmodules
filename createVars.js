const fs = require('fs');
var request = require('request');
/*
this creates all the necessary variables to have working CI/CD
*/
path = "D:/dhonzawa/Desktop/guaripolo/bob/"
gitlabVariables = [
    { setupName: "ASC_APP_ID" },
    { setupName: "APP_SPECIFIC_PASSWORD" },
    { setupName: "CLIENT_ID" },
    { setupName: "SENTRY_DSN" }
]
//ok so fyi we are gonna parse the app-setup file and get the variables 
/* variable example
{
    "variable_type": "env_var",
    "key": gitlabVariables[0].setupName,
    "value": gitlabVariables[0].value,
    "protected": false,
    "masked": false,
    "environment_scope": "*"
}
*/

function readENV() {
    if (fs.existsSync(path + ".env")) {
        envFile = fs.readFileSync(path + ".env", "utf8")
        console.log(envFile)
        sendVars()

    } else {
        throw new Error("There's no .env file D:");
    }

}

function sendVars() {
    gitlabVariables.forEach(item => {
        if (item.setupName === "CLIENT_ID") {
            isProtected = false
        } else {
            isProtected = true
        }
        var options = {
            'method': 'POST',
            'url': 'https://gitlab.com/api/v4/projects/39066736/variables',
            'headers': {
                'PRIVATE-TOKEN': 'TOKEN!!',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "variable_type": "env_var",
                "key": item.setupName,
                "value": item.value,
                "protected": isProtected,
                "masked": false,
                "environment_scope": "*"
            })
        };
        //im not making a request for each variable bc I want to btw

        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
        });

        //now send both files, eas and .env     
        options.body = JSON.stringify({
            "variable_type": "file",
            "key": "ENV_MASTER",
            "value": envFile,
            "protected": true,
            "masked": false,
            "environment_scope": "*"
        })
        request(options, function (error, response) {
            if (error) throw new Error(error);
            console.log(response.body);
        });
        options.body = JSON.stringify({
            "variable_type": "file",
            "key": "EAS_JSON",
            "value": appJsonFile,
            "protected": true,
            "masked": false,
            "environment_scope": "*"
        })
    })

}

function readJSON() {
    //will parse file and send eas.json
    appJsonFile = JSON.parse(fs.readFileSync(path + "easTemplate.json", 'utf8'));
    readENV()
}

function readAppSetupFile() {
    var appSetupFile = fs.readFileSync(path + "app-setup.txt").toString().split("\n");
    appSetupFile.forEach(line => {
        gitlabVariables.forEach(item => {
            if (line.includes(item.setupName)) {
                item.value = line.split("=")[1]
            }
        })

    });
    //iterate through file, put each value into object yadda yadda
    readJSON()
}

readAppSetupFile()