// server.js
const cors = require('cors');
const multer = require("multer");
path = "D:/dhonzawa/Desktop/guaripolo/bob/sv/uploads"
var filesNeeded = ["icon.png", "launcher.png", "logo.png", "app-setup.txt", "google-services.json", "GoogleService-Info.plist"]
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
})
const upload = multer({ storage: storage })
const express = require("express");
const app = express();
app.use(express.json());
app.use(cors())
app.use(express.urlencoded({ extended: true }));
app.post("/upload_files", upload.array("files"), uploadFiles);

function uploadFiles(req, res) {
    if (req.files.length === 0) {
        //res.statusCode = 400
        res.json({ statusCode: 400 })
    } else {
        //req.files
        for (let index = 0; index < req.files.length; index++) {
            const element = req.files[index];
            if (filesNeeded.includes(element.filename)) {
            } else {
                res.json({ statusCode: 400 })

            }

        }
        res.json({ message: "Successfully uploaded files" });

    }
}
app.listen(1337, () => {
    console.log(`Server started...`);
});



//Uploading multiple files
