# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [6.19.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.18.0...v6.19.0) (2022-08-22)

## [6.18.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.17.0...v6.18.0) (2022-08-22)

## [6.17.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.16.0...v6.17.0) (2022-08-22)


### Features

* initial customization ([b668e2f](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/b668e2ffe2a09fcfac342aa3b50a2593774e020e))

## [6.16.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.15.1...v6.16.0) (2022-08-15)

### [6.15.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.15.0...v6.15.1) (2022-07-15)

## [6.15.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.14.0...v6.15.0) (2022-07-14)


### Features

* reverting hacky analytics ([44e362c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/44e362c738671c0bafa1c06d4171eedcb9f58ca3))

## [6.14.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.12.0...v6.14.0) (2022-07-13)


### Features

* comment out send message and invite buttons ([34f0a36](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/34f0a3662b11c211cc4ca1ee3be873fa4fa85c73))
* comment out tests for commented out code ([fad9eb2](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/fad9eb299eb09507ace80eddab16cd58bcb794a4))
* initial google analytics push ([4c2d598](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/4c2d5986ab90896a6bfbd151c4990964d079fa58))
* update snapshot to match test changes ([8ef09b2](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/8ef09b275aa9e83f51b47de55024aa7c7f6b284e))

### [6.13.5](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.13.2...v6.13.5) (2022-07-12)

### [6.13.4](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.13.2...v6.13.4) (2022-06-17)

### [6.13.3](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.13.2...v6.13.3) (2022-06-17)

### [6.13.2](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.13.1...v6.13.2) (2022-04-14)

### [6.13.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.13.0...v6.13.1) (2022-04-06)

## [6.13.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.12.2...v6.13.0) (2022-04-05)


### Features

* update snapshot to match test changes ([8ef09b2](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/8ef09b275aa9e83f51b47de55024aa7c7f6b284e))

### [6.12.2](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.12.1...v6.12.2) (2022-04-05)


### Features

* comment out tests for commented out code ([fad9eb2](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/fad9eb299eb09507ace80eddab16cd58bcb794a4))

### [6.12.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.12.0...v6.12.1) (2022-04-04)


### Features

* comment out send message and invite buttons ([34f0a36](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/34f0a3662b11c211cc4ca1ee3be873fa4fa85c73))

## [6.12.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.11.0...v6.12.0) (2021-11-19)


### Features

* small commit to update tags ([6051be3](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/6051be33777f168c0a69895e882e5fc7a23199c4))

## [6.11.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.10.0...v6.11.0) (2021-11-18)


### Features

* fixing ordering for events and my items ([d7ceba2](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d7ceba2e7522d524c049fab1854c8013529198b7))
* merge conflict ([7e230fd](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/7e230fd5da3382f676495023d6b099d871142f4f))

## [6.10.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.9.0...v6.10.0) (2021-11-15)


### Features

* updating a package; fixing missing dependency in CreateComment ([6bcbadf](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/6bcbadfc583e6f0051cd703e46a17e44326cb6c3))
### [6.9.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.9.0...v6.9.1) (2021-11-15)

## [6.9.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.8.0...v6.9.0) (2021-11-12)


### Features

* another place to update photo upload issue ([cc36e93](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/cc36e93c3da5524cebe5e266d6804b5aa17802c0))

## [6.8.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.7.0...v6.8.0) (2021-11-10)


### Features

* adding some unmet dependencies; tweaking activity feed to match challenges and hopefully fix it; disabling tests for activity feed since the interface changed and they broke ([a9a8391](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/a9a839129a48d458b09d2932e21c2facce617e57))

## [6.7.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.6.0...v6.7.0) (2021-11-10)


### Features

* minor version updates to expo-updates and react-native; redoing interface for challenge photos; removing tests for challenge photos temporarily; additional logging in various places ([5b58657](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/5b58657a93e918411ef763d989f5205bf7dcfc81))

## [6.6.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.5.0...v6.6.0) (2021-11-09)

## [6.5.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.4.0...v6.5.0) (2021-11-09)


### Features

* small tweak to add default config for picker before changing interface ([82d652d](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/82d652d612e628ed948698bd63eed1bf33cc785a))

## [6.4.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.3.0...v6.4.0) (2021-11-09)


### Features

* tweaks to tests ([4f7e7e2](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/4f7e7e2ba2c7e20575a61af1711455d7208d00cb))

## [6.3.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.2.0...v6.3.0) (2021-11-09)


### Features

* tweaking photo challenges to pass tests ([f2140e7](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/f2140e7172aa042720699b6308af27c88d1c1b8b))

## [6.2.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.1.0...v6.2.0) (2021-11-08)


### Features

* updating to hermes, updating tests, updating deprecated permissions request, tweaks to photo challenge to try to resolve crash, fixing tests so they dont break every month ([0fdc4b1](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/0fdc4b1381a232b4235a5b35fe980e81a89769bf))

## [6.1.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v6.0.0...v6.1.0) (2021-11-05)


### Features

* updating tests ([cea2ccd](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/cea2ccd0eca9660c15b6bfc033e15fb17a747204))

## [6.0.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.33.0...v6.0.0) (2021-11-05)


### Features

* add support for multiple expo tokens by event ([06d2291](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/06d22913231afbbad396cb61aa292b2cf395f35d))
* added colors and logo only ([daf0055](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/daf00555815fd443afb0a53af9fb8abb53a3974c))
* EA2-962 hiding the MainHeader on Forgot Password ([5ae8747](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/5ae87471d346be5ed2c0cea63b38aad508b38774))
* EA2-962 hiding the MainHeader on Forgot Password ([61e0565](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/61e0565b6f615ffcf1ed8d092c2393d17cff5c20))
* EA2-962 reverting some of the CSS changes ([b5ddf96](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/b5ddf96a36d9b79fb7051f33ee2fc0f9e65835e3))
* updating expo sdk to 43 ([48591fb](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/48591fbde31abfa3e96e6b5b572cfce02fd6a541))

## [4.10.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.4.0...v4.10.0) (2021-07-06)


### Features

* pushing to EA2-954 to QA fixing tests ([33bb0a9](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/33bb0a9845b06db2846b4e9e4970f94fc875f717))
* pushing to EA2-954 to QA fixing tests ([e87fdc6](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/e87fdc6597fea5fa9dc9462a6efe8643471f5f29))
* updated images, spacing and work for EA2-954 ([7b95779](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/7b95779e0b8129fb7e6a7d4c132970a6ca3df02c))

## [4.4.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.3.0...v4.4.0) (2021-06-30)


### Bug Fixes

* picker package was migrated, updating package.json and yarn.lock ([d2ec47a](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d2ec47a33a570160e209e039c4d063f7eadbf81f))

## [4.3.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.2.0...v4.3.0) (2021-06-30)


### Bug Fixes

* typo in gitlab variable for sentry dsn ([0ddc45c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/0ddc45c732012d00d23be09c69f8751846a02d33))

## [4.2.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.1.0...v4.2.0) (2021-06-30)


### Bug Fixes

* testing a theory that sentry dsn is getting populated by the eas var during publish, which shouldnt happen because publish is still an expo task, not an eas one ([9db186b](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/9db186b62abcb2196056a685cfed80528a95249a))

## [4.1.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.0.0...v4.1.0) (2021-06-30)


### Bug Fixes

* updating gitlab yaml, merge conflicts ([ee3971d](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/ee3971de86f462530c36d0035e0f2b4252ba5c10))

## [3.7.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.6.0...v3.7.0) (2021-06-29)

### [5.33.3](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.33.2...v5.33.3) (2021-10-22)


### Features

* add support for multiple expo tokens by event ([06d2291](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/06d22913231afbbad396cb61aa292b2cf395f35d))

### [5.33.2](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.33.1...v5.33.2) (2021-10-04)

### [5.33.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.33.0...v5.33.1) (2021-10-04)

## [4.13.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.12.0...v4.13.0) (2021-07-18)


### Features

* added colors and logo only ([daf0055](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/daf00555815fd443afb0a53af9fb8abb53a3974c))

## [4.12.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.11.0...v4.12.0) (2021-07-17)

## [4.11.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.10.0...v4.11.0) (2021-07-17)


### Features

* EA2-962 hiding the MainHeader on Forgot Password ([5ae8747](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/5ae87471d346be5ed2c0cea63b38aad508b38774))
* EA2-962 hiding the MainHeader on Forgot Password ([61e0565](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/61e0565b6f615ffcf1ed8d092c2393d17cff5c20))
* EA2-962 reverting some of the CSS changes ([b5ddf96](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/b5ddf96a36d9b79fb7051f33ee2fc0f9e65835e3))

## [4.10.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.9.0...v4.10.0) (2021-07-06)

## [4.9.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.5.0...v4.9.0) (2021-07-06)


### Features

* pushing to EA2-954 to QA fixing tests ([33bb0a9](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/33bb0a9845b06db2846b4e9e4970f94fc875f717))
* pushing to EA2-954 to QA fixing tests ([e87fdc6](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/e87fdc6597fea5fa9dc9462a6efe8643471f5f29))

## [4.5.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.4.0...v4.5.0) (2021-07-06)


### Features

* updated images, spacing and work for EA2-954 ([7b95779](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/7b95779e0b8129fb7e6a7d4c132970a6ca3df02c))

## [4.4.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.3.0...v4.4.0) (2021-06-30)


### Bug Fixes

* picker package was migrated, updating package.json and yarn.lock ([d2ec47a](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d2ec47a33a570160e209e039c4d063f7eadbf81f))

## [4.3.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.2.0...v4.3.0) (2021-06-30)


### Bug Fixes

* typo in gitlab variable for sentry dsn ([0ddc45c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/0ddc45c732012d00d23be09c69f8751846a02d33))

## [4.2.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.1.0...v4.2.0) (2021-06-30)


### Bug Fixes

* testing a theory that sentry dsn is getting populated by the eas var during publish, which shouldnt happen because publish is still an expo task, not an eas one ([9db186b](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/9db186b62abcb2196056a685cfed80528a95249a))

## [4.1.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.0.0...v4.1.0) (2021-06-30)


### Bug Fixes

* updating gitlab yaml, merge conflicts ([ee3971d](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/ee3971de86f462530c36d0035e0f2b4252ba5c10))

## [3.7.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.6.0...v3.7.0) (2021-06-29)

## [5.33.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.32.0...v5.33.0) (2021-09-22)

## [5.32.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.31.0...v5.32.0) (2021-09-21)

## [5.31.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.30.0...v5.31.0) (2021-09-21)

## [5.30.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.29.0...v5.30.0) (2021-09-21)

## [5.29.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.28.0...v5.29.0) (2021-09-17)


### Features

* grrr another attempt to update for eas cli changes ([51f1618](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/51f16181dea490d2c6cbea3cf388f2baab52dca3))

## [5.28.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.27.0...v5.28.0) (2021-09-17)


### Features

* grrr cicd update for eas cli changes ([0d63859](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/0d63859fa20f123f5137d9e0002e892c0fa4cf7c))

## [5.27.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.26.0...v5.27.0) (2021-09-17)


### Features

* even more changes cicd update for eas cli changes ([2e7a5b6](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/2e7a5b6fad52301be40132cf72854057579bcc69))

## [5.26.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.25.0...v5.26.0) (2021-09-17)


### Features

* more changes cicd update for eas cli changes ([bed54ad](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/bed54adee134f1740ac92c193a196ded1f76b3ea))

## [5.25.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.24.0...v5.25.0) (2021-09-17)


### Features

* changes and another approach to the cicd update for eas cli changes ([d29d67c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d29d67c57ed9ecfb96be6e8bb74e1f71c2402dc0))

## [5.24.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.23.0...v5.24.0) (2021-09-17)


### Features

* cicd update for eas cli changes ([ce57dd5](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/ce57dd5c15a5928da65d59e4de87aa5af085aede))

## [5.23.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.22.0...v5.23.0) (2021-09-17)


### Features

* cicd update for eas cli changes ([5bd000a](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/5bd000af851d22d2883bfe8e74be56f2805c0137))

## [5.22.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.21.0...v5.22.0) (2021-09-17)


### Features

* EA2-982 fix attempts 4 ([6fe393a](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/6fe393a4725f8c1023b83fa2008d6025a2fce843))

## [5.21.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.20.0...v5.21.0) (2021-09-17)


### Features

* EA2-982 fix attempts 3 ([163802d](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/163802d753ce458db7ee5d172f573d81667d59fa))

## [5.20.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.19.0...v5.20.0) (2021-09-16)


### Features

* EA2-982 fix attempts 2 ([3a1e193](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/3a1e193fad2aa34b30dff8f5db557cc313633b2b))

## [5.19.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.18.0...v5.19.0) (2021-09-16)


### Features

* Add notifications count to memory state, set count from EventHome ([bfe7967](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/bfe79674d0691a114c33fff9adeb007b6c93dfa5))
* EA2-982 fix attempts ([768dcb5](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/768dcb5d56115875ef2b3ed5c0ee153873e37eb9))
* Set badge for notification icon ([ca15da5](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/ca15da59594ab369c26fa752e689db3074b6dd3d))

### [5.18.2](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.18.1...v5.18.2) (2021-09-15)


### Features

* Set badge for notification icon ([ca15da5](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/ca15da59594ab369c26fa752e689db3074b6dd3d))

### [5.18.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.18.0...v5.18.1) (2021-09-15)


### Features

* Add notifications count to memory state, set count from EventHome ([bfe7967](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/bfe79674d0691a114c33fff9adeb007b6c93dfa5))

## [5.18.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.17.0...v5.18.0) (2021-09-15)


### Features

* updating cicd to use eas.json from gitlab vars for submit; gitignoring some things ([e32eb53](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/e32eb531ad9dd52fdc0f4227afe01eabef8d938d))

## [5.17.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.12.0...v5.17.0) (2021-09-15)


### Features

* fixing EA2-980 ([a965141](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/a965141791a414a82d1f371cdafff48b72f01208))
* Update NotificationIcon badge count in Header ([1936062](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/1936062e4e2e582d7924b7014e19ffbf5a00b425))

### [5.16.3](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.16.2...v5.16.3) (2021-09-13)


### Features

* Update NotificationIcon badge count in Header ([1936062](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/1936062e4e2e582d7924b7014e19ffbf5a00b425))

### [5.16.2](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.16.1...v5.16.2) (2021-09-13)

### [5.16.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.16.0...v5.16.1) (2021-09-10)

## [5.16.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.15.1...v5.16.0) (2021-09-10)

### [5.15.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.15.0...v5.15.1) (2021-09-10)

## [5.15.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.13.2...v5.15.0) (2021-09-09)

## [5.14.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.13.2...v5.14.0) (2021-09-09)

### [5.13.2](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.13.1...v5.13.2) (2021-09-08)

### [5.13.1](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.13.0...v5.13.1) (2021-09-08)

## [5.13.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.12.0...v5.13.0) (2021-08-25)

## [5.12.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.8.0...v5.12.0) (2021-08-18)


### Features

* basic fcm setup for android ([d2dc8a6](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d2dc8a6c1621403b274e9fe7bdd9259db6c97611))
* EA2-954 login and lost password theming ([d80270c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d80270c546bbcd8e5d101aa0420b2effc91c4285))
* Updated login button on selection ([ac86b9d](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/ac86b9d618894b026cf2d741c99210af8122bf5a))
* Updated login button on selection ([9745fc8](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/9745fc83fd23b6c9c372884212e271ca506a6390))
* Updated login button on selection ([9e15966](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/9e1596621b565d32a7e862f6081790bf7deab2f1))

## [5.11.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.10.0...v5.11.0) (2021-08-12)

## [5.10.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.8.0...v5.10.0) (2021-08-05)


### Features

* EA2-954 login and lost password theming ([d80270c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d80270c546bbcd8e5d101aa0420b2effc91c4285))
* Updated login button on selection ([ac86b9d](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/ac86b9d618894b026cf2d741c99210af8122bf5a))
* Updated login button on selection ([9745fc8](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/9745fc83fd23b6c9c372884212e271ca506a6390))
* Updated login button on selection ([9e15966](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/9e1596621b565d32a7e862f6081790bf7deab2f1))

## [5.9.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.8.0...v5.9.0) (2021-07-20)

## [5.8.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.7.0...v5.8.0) (2021-07-20)


### Bug Fixes

* reverting all my testing since nothing worked ([f14990a](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/f14990ad535bd0e05773f4d1b9ed09d5a056858c))

## [5.7.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.6.0...v5.7.0) (2021-07-20)


### Bug Fixes

* adding a bunch of images in a couple different ways to see if anything works ([0957ebb](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/0957ebb7d40eb775157bb77836cf630df721edee))

## [5.6.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.5.0...v5.6.0) (2021-07-20)


### Bug Fixes

* updating tests and tweaking style ([2b41049](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/2b410499e21891df02cc2349322fb3c71dbbce4b))

## [5.5.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.4.0...v5.5.0) (2021-07-20)


### Bug Fixes

* hardcoding width and height for login page elements ([1be24db](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/1be24db8d5f794f93d9601c7579ffc34b339986e))

## [5.4.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.3.0...v5.4.0) (2021-07-20)


### Bug Fixes

* another reference to logo needed to be updated to test jpg ([46066ae](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/46066ae6ec023d280c22ef64c495ff04a43ffc9f))

## [5.3.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.2.0...v5.3.0) (2021-07-19)


### Bug Fixes

* duplicate asset issue for android; tweak to environments ([34736a7](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/34736a752d4d784198cd07ad88ba92e24dd19355))

## [5.2.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.1.0...v5.2.0) (2021-07-19)


### Bug Fixes

* trying a jpg; also consolidating the submit step to keep stores in sync ([d38050a](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d38050a8c614ddd120b6ee605ad751a14e665e1d))

## [5.1.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v5.0.0...v5.1.0) (2021-07-19)


### Bug Fixes

* tweak to bundling ([da25cee](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/da25ceec94e6df7fa8af90ed1b19cc6da1a53078))

## [5.0.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v4.0.0...v5.0.0) (2021-07-19)


### Bug Fixes

* reverting to tag 4.0.0 to see if that works; will push as a major version ([f1b845b](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/f1b845b73712830dd96f9db8c26b2199192b2a74))

## [4.0.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.6.0...v4.0.0) (2021-06-30)


### Bug Fixes

* aws links, extrapolating client id, removing build.js ([e379140](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/e3791405761a9412fdfce4f7ebaa8a0818741b0e))

## [3.6.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.5.0...v3.6.0) (2021-06-29)


### Features

* extrapolating some config vars fix: replacing app.js SENTRY_DSN value with process.env version ([faaac6b](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/faaac6b02452317e99dbe059d6a2bc35ff2de67f))


### Bug Fixes

* merge conflicts since I forgot to merge down master ([d03fd74](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/d03fd74bc9597135ff7db9b979af038c1034d650))

## [3.5.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.4.0...v3.5.0) (2021-06-24)


### Features

* fully automated pipeline from commit to test to publish to build to submit working; tweaks to optimize cicd times ([fe3d02c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/fe3d02cfe33aff09893c1d9f7c019bdde821c353))

## [3.4.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.3.0...v3.4.0) (2021-06-24)


### Bug Fixes

* tweaks to cicd and readme ([40cbe3b](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/40cbe3b831b82d3a180c2a98c4978f8df82990cd))

## [3.3.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.2.0...v3.3.0) (2021-06-24)


### Bug Fixes

* more eas tweaks ([0d2079c](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/0d2079cc5bf08d3f6a2cc2de73020a75544fdd7b))

## [3.2.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.1.0...v3.2.0) (2021-06-24)


### Bug Fixes

* updating cicd and entering stuff into readme ([92bb21d](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/92bb21d13e8c54060743bf20fd91e9f2aa5aebbf))

## [3.1.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v3.0.0...v3.1.0) (2021-06-24)


### Bug Fixes

* trying to fix args that dont work anymore ([fc3e203](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/fc3e2033a193d8f14802e217ba9d3b275312c59e))

## [3.0.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v2.2.0...v3.0.0) (2021-06-24)


### Bug Fixes

* messing around with environments because of stupid changes ([8523e1b](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/8523e1bbe90efcd4f2986fb57e3f2b8e0d80ce2b))

## [2.2.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v2.1.0...v2.2.0) (2021-06-24)


### Bug Fixes

* simplifying cicd pipeline (publish and build in the same step) ([111adf2](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/111adf2047f8fbed2185b92f0cf1a9ac318c78e7))

## [2.1.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v2.0.0...v2.1.0) (2021-06-24)


### Bug Fixes

* eas builds need git apparently ([32319c5](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/32319c5e1f8228ebd93604f59e32bb041b92dd53))

## [2.0.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.11.0...v2.0.0) (2021-06-24)


### Bug Fixes

* eas pipeline full test; adding sentry dsn to eas.json ([2d09b94](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/2d09b9492472f5b4eafd28a3631660b975142c7f))

## [1.11.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.10.0...v1.11.0) (2021-06-24)


### Bug Fixes

* eas builds, tests, automation ([4cb6faf](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/4cb6fafe1d1f0b64de1878285a19b9e4b0779d2f))

## [1.10.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.9.0...v1.10.0) (2021-06-24)

## [1.9.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.8.0...v1.9.0) (2021-06-24)

## [1.8.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.7.0...v1.8.0) (2021-06-23)


### Bug Fixes

* fixing various icons and icon widths; fixing tests ([1a62a81](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/1a62a8195c948d582b77d9e104b1c07b33c062ef))

## [1.7.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.6.0...v1.7.0) (2021-06-23)


### Bug Fixes

* fixing icons and user agendas and tests ([42ac80a](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/42ac80a6a5d0111d744afd82908a3d0ef5fd2e2e))

## [1.6.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.5.0...v1.6.0) (2021-06-23)


### Bug Fixes

* fixing problem with redundant names in dependencies (fixes ios issue) ([9f7bd3f](https://gitlab.com/creativegroup/event-app-2/event-app-2/commit/9f7bd3fd1973fac98ee8bfeb2230d829bcfdce37))

## [1.5.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.4.0...v1.5.0) (2021-06-21)

## [1.4.0](https://gitlab.com/creativegroup/event-app-2/event-app-2/compare/v1.3.0...v1.4.0) (2021-06-19)

## 1.3.0 (2021-06-19)
