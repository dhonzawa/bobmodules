module.exports = function (api) {
  api.cache(false);
  return {
    presets: ['babel-preset-expo', 'module:metro-react-native-babel-preset'],
    plugins: [
      'inline-dotenv',
      'react-native-reanimated/plugin',
      [
        'module-resolver',
        {
          root: ['.'],
          extensions: ['.ios.ts', '.android.ts', '.ts', '.ios.tsx', '.android.tsx', '.tsx', '.jsx', '.js', '.json'],
          alias: {
            _config: './src/config',
            _assets: './assets',
            _components: './src/components',
            _contexts: './src/contexts',
            _hooks: './src/hooks',
            _navigators: './src/navigators',
            _screens: './src/screens',
            _utils: './src/utils',
            _validators: './src/validators',
            _enums: './src/enums',
            _theme: './src/theme',
            _build: './build',
            _mocks: './tests/mocks',
            _mocksOld: './tests/mocksOld',
          },
        },
      ],
    ],
  };
};
