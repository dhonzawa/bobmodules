import React from 'react';
import { LogBox } from 'react-native';
import 'react-native-gesture-handler';
import { Root, StyleProvider } from 'native-base';

import getTheme from '_theme/components';
import variables from '_theme/variables/commonColor';
import MainNavigator from '_navigators/MainNavigator';
import { AppProvider } from '_contexts/AppContext';
import { DataProvider } from '_contexts/DataContext';
import { UiProvider } from '_contexts/UiContext';
import PushNotifications from '_components/PushNotifications';

import * as Sentry from 'sentry-expo';

Sentry.init({
  dsn: process.env.SENTRY_DSN,
  enableInExpoDevelopment: true,
  debug: true
});

const App = () => {
  LogBox.ignoreLogs([
    'YellowBox has been replaced',
    'Deprecation warning: value provided',
    'Cannot update a component from inside',
    'VirtualizedLists should never',
    'expo-permissions is now deprecated',
    'Can\'t perform a React state update',
    'Failed prop type',
  ]);

  return (
    <StyleProvider style={getTheme(variables)}>
      <Root>
        <AppProvider>
          <DataProvider>
            <UiProvider>
              <PushNotifications />
              <MainNavigator />
            </UiProvider>
          </DataProvider>
        </AppProvider>
      </Root>
    </StyleProvider>
  );
};
export default App;
