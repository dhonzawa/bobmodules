const fs = require('fs');
colorFilePath = "D:/dhonzawa/Desktop/guaripolo/bob/colorTemplate.js"
path = "D:/dhonzawa/Desktop/guaripolo/bob/"
destination = "D:/dhonzawa/Desktop/guaripolo/bob/newBuild/"
appJSON = {}
var appJsonFile
var colorFile
//contains all data need to customize the app that's behind equal signs, (API keys, etc)
const appInfo = [{ setupName: "Project Name", stdName: "slug" }, { setupName: "Official", stdName: "name" },
{ setupName: "GoogleMaps", stdName: "apiKey" }, { setupName: "bundle", stdName: "package" }, { setupName: "bundle", stdName: "bundleidentifier" },
{ setupName: "#Short App", stdName: "description" }]
//contains all customizable colors
const customColors = [{ name: "authBkg" }, { name: "authSubtitle" }, { name: "authFieldColor" }, { name: "authFieldInput" }, { name: "authBtnBkg" },
{ name: "authBtnBorder" }, { name: "authBtnTxt" }, { name: "authLgnPasLinks" }, { name: "authTnC" }, { name: "authSpinner" }]


function modifyAppJSON() {
    appInfo.forEach(value => {
        if (value.stdName === 'slug') {
            appJsonFile.expo.slug = value.splitName
            appJsonFile.expo.android.package = "com.creativegroup." + value.splitName
            appJsonFile.expo.ios.bundleIdentifier = "com.creativegroup." + value.splitName
            appJsonFile.expo.hooks.postPublish[0].config.project = value.splitName
        }
        if (value.stdName === "description") {
            appJsonFile.expo.description = value.splitName
        }
        if (value.stdName === "apiKey") {
            //console.log(appJsonFile.expo.android.config.googleMaps.apiKey)
            appJsonFile.expo.android.config.googleMaps.apiKey = value.splitName
            appJsonFile.expo.ios.config.googleMapsApiKey = value.splitName
            /*
            this is not working, I think it might be related to the changes EA2 is going through?
            appJsonFile.expo.web.config.firebase.apiKey = value.splitName
            */
        }
        console.log(value.splitName)
        fs.writeFileSync(destination + "app.json", JSON.stringify(appJsonFile), 'utf8');

    })
    modifyColorsFile()

}

function modifyColorsFile() {
    customColors.forEach(color => {
        for (let line = 1; line < colorFile.length - 4; line++) {
            if (colorFile[line].includes(color.name)) {
                temp = colorFile[line].split("//")
                temp[0] = "    " + color.name + ": " + color.color
                colorFile[line] = temp[0] + " //" + temp[1]
            }
        }

    })
    //now just write a file
    colorFile.forEach(line => {
        //fs.appendFileSync(destination + "colors.js", line.toString(), 'utf8');
    })
    //googleServicePlist()
}

function readAppJson() {
    appJsonFile = JSON.parse(fs.readFileSync(path + "templateApp.json", 'utf8'));
    readColorsJS()
}

function readAppSetupFile() {
    var appSetupFile = fs.readFileSync(path + "app-setup.txt").toString().split("\n");
    //iterate through file, put each line into array 
    appSetupFile.forEach(line => {
        //iterate through lines, split them if they contain an equal sign (new format for app-setup.txt?)
        appInfo.forEach(appKeys => {
            if (line.includes(appKeys.setupName)) {
                //push an object with the following keys: line.split("=")
                a = line.split("=")
                appKeys.splitName = a[1]
                
            }
        })
        //reads the colors
        customColors.forEach(colorSetting => {
            if (line.includes(colorSetting.name)) {
                //if line contains then split it by the ":" and push it
                colorSetting.color = line.split(":")[1]
            }

        })
    });
    readAppJson()
}

function googleServicePlist() {
    //moves GoogleService-Info.plist to /newBuild
    //renames google-services.json to fcm.services.json and moves it to newbuild
    let variableFiles = ["GoogleService-Info.plist", "google-services.json"]
    variableFiles.forEach(file => {
        if (fs.existsSync(path + file)) {


        } else {
            throw new Error(file + " does not exist");
        }

    })
    fs.renameSync(path + "GoogleService-Info.plist", destination + "GoogleService-Info.plist")
    fs.renameSync(path + "google-services.json", destination + "fcm.services.json")


}

function readColorsJS() {
    //reads the files
    colorFile = fs.readFileSync(path + "colorTemplate.js").toString().split("\n");
    modifyAppJSON()
}
readAppSetupFile()
