var request = require('request');
const fs = require('fs');
const readline = require('readline-sync');
/*
So what this does is: creates a new export in gitlab, does long polling to check the status (it's not even close to fast btw) and then retrieves the export file.
Once the export file is downloaded, we create a new project using it as a template
*/
const options = {
    url: 'https://gitlab.com/api/v4/projects/21161726/export',
    headers: { 'PRIVATE-TOKEN': 'TOKEN!!' },
}
//makes POST request to create a new export
function createNewExport() {
    request.post(options, function (error, response, body) {
        if (!error && response.statusCode === 202) {
            console.log(body) // Print the req.body
            requestStatus() //calls the export status check
        }
    })
}

//check the export status
function requestStatus() {
    request(options, function (error, response, body) {
        if (!error) {
            checkResponse(body)
        }

    })
}
function checkResponse(body) {
    body = JSON.parse(body)
    console.log(body.export_status)
    if (body.export_status != 'finished') {
        //console.log(body.export_status)
        setTimeout(function () {
            requestStatus();
            console.log('request sent after 1 minute.')
        }, 60000);

    } else {
        console.log("it's done! " + body.export_status)
        retrieveExportFile()
    }
}

function retrieveExportFile() {
    options.url = "https://gitlab.com/api/v4/projects/21161726/export/download"
    request(options, function (error, response, body) {
        if (!error) {
            sendExportFile()
        }
    }).pipe(fs.createWriteStream('projectFile.tar.gz'))
}

function sendExportFile() {
    let projectName = readline.question("What's the slug of the project? "); //this will need to be changed to read it from the app-setup file
    var importOptions = {
        'method': 'POST',
        'url': 'https://gitlab.com/api/v4/projects/import',
        'headers': {
            'PRIVATE-TOKEN': 'glpat--AeCn4ATtU6Xi_awzwoz' 
        },
        formData: {
            'file': {
                'value': fs.createReadStream('D:/dhonzawa/Desktop/bob/bob/projectFile.tar.gz'),
                'options': {
                    'filename': 'D:/dhonzawa/Desktop/bob/bob/projectFile.tar.gz',
                    'contentType': null
                }
            },
            'path': projectName,
            'namespace': 'creativegroup/event-app-2/events'
        }
    };
    request(importOptions, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });
}

function importProject() {
    createNewExport()
}
sendExportFile()
//importProject()
