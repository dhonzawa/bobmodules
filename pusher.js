const fs = require('fs');
var request = require('request');
function pushCommit() {
    var path = "D:/dhonzawa/Desktop/guaripolo/bob/newBuild/"
    const iconFile = fs.readFileSync(path + "icon.png", { encoding: 'base64' });
    const launcherFile = fs.readFileSync(path + "launcher.png", { encoding: 'base64' });
    const logoFile = fs.readFileSync(path + "logo.png", { encoding: 'base64' });
    appJson = JSON.parse(fs.readFileSync(path + "app.json", 'utf8'));
    //read googleservice and fcm.services
    const googleService = fs.readFileSync(path + "GoogleService-Info.plist", "utf-8");
    const fcmService = fs.readFileSync(path + "fcm.services.json", 'utf-8');
    const colorFile = fs.readFileSync(path + "colors.js", 'utf-8');
    JSONpayload = JSON.stringify(appJson)
    var options = {
        'method': 'POST',
        'url': 'https://gitlab.com/api/v4/projects/39036367/repository/commits',
        'headers': {
            'PRIVATE-TOKEN': 'TOKEN!!',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            "branch": "master",
            "commit_message": "feat: initial customization, rebuild -Bob :)",
            "actions": [
                {
                    "action": "update",
                    "file_path": "assets/variables/icon.png",
                    "encoding": "base64",
                    "content": iconFile
                },
                {
                    "action": "update",
                    "file_path": "assets/variables/launcher.png",
                    "encoding": "base64",
                    "content": launcherFile
                },
                {
                    "action": "update",
                    "file_path": "assets/variables/logo.png",
                    "encoding": "base64",
                    "content": logoFile
                },
                {
                    "action": "update",
                    "file_path": "app.json",
                    "encoding": "text",
                    "content": JSONpayload
                },
                {
                    "action": "update",
                    "file_path": "fcm.services.json",
                    "encoding": "text",
                    "content": googleService
                },
                {
                    "action": "update",
                    "file_path": "GoogleService-Info.plist",
                    "encoding": "text",
                    "content": fcmService
                },
                {
                    "action": "update",
                    "file_path": "GoogleService-Info.plist",
                    "encoding": "text",
                    "content": colorFile
                }
            ]
        })

    };
    request(options, function (error, response) {
        if (error) throw new Error(error);
        console.log(response.body);
    });

}




